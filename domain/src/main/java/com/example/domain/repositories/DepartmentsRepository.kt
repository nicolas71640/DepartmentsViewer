package com.example.domain.repositories

import com.example.domain.entities.DepartmentEntity
import com.example.domain.entities.TownEntity
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

/**
 * Repository Interface needed to implement Dependency Inversion of clean architecture
 *  Needed to be implemented in Data module
 *
 */
interface DepartmentsRepository {
    fun getDepartments(): Flowable<List<DepartmentEntity>>
    fun getDepartment(code: String): Single<DepartmentEntity>
    fun getDepartmentTowns(code : String): Flowable<List<TownEntity>>
    fun saveDepartment(departmentEntity: DepartmentEntity): Completable
}