package com.example.domain.usecases

import com.example.domain.entities.DepartmentEntity
import com.example.domain.entities.TownEntity
import com.example.domain.repositories.DepartmentsRepository
import io.reactivex.Flowable
import io.reactivex.Single

class GetDepartmentsUseCase(private val repository: DepartmentsRepository) {
    /**
     * Get All Departments
     *
     * @return  List of DepartmentEntity (Flowable)
     */
    fun getDepartments():Flowable<List<DepartmentEntity>>
    {
        return repository.getDepartments()
    }

    /**
     * Get department instance based on its code.
     * Get Department instance, then its towns, then save them to the cache
     *
     * @param code  department code
     * @return      Department Entity (Flowable)
     */
    fun getDepartment(code:String): Single<DepartmentEntity> {
        return repository.getDepartment(code)
            .flatMap { departmentEntity: DepartmentEntity ->
                //If details are already saved, no need to get towns from remote
                if(departmentEntity.detailsSaved) Single.just(departmentEntity)
                else {
                    repository.getDepartmentTowns(code)
                        .flatMap { townEntities: List<TownEntity> ->
                            //Towns Number
                            departmentEntity.townsNumber = townEntities.size.toLong()
                            var inhabitants: Long = 0

                            //Department Inhabitants
                            townEntities.forEach { inhabitants += it.inhabitants }
                            departmentEntity.inhabitants = inhabitants
                            repository.saveDepartment(departmentEntity)
                                .andThen(Flowable.just(departmentEntity))
                        }
                        .first(departmentEntity)
                        .onErrorResumeNext(Single.just(departmentEntity))
                }
            }
    }
}