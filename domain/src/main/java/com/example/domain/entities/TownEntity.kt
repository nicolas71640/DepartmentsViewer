package com.example.domain.entities

data class TownEntity (val name: String, val code: String, val codeDepartment: String, val codeRegion: Int, val inhabitants: Long){
}