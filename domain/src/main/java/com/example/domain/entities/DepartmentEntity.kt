package com.example.domain.entities

data class DepartmentEntity (val name: String,val code: String, val codeRegion: Int,var detailsSaved:Boolean = false,var inhabitants:Long = 0, var townsNumber:Long = 0){

}