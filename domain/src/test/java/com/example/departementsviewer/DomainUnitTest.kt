package com.example.departementsviewer

import com.example.domain.entities.DepartmentEntity
import com.example.domain.usecases.GetDepartmentsUseCase
import io.reactivex.schedulers.Schedulers
import io.reactivex.subscribers.TestSubscriber
import org.junit.Test

class DomainUnitTest {


    @Test
    fun getDepartmentsUseCasesTest() {
        val getDepartmentsUseCases = GetDepartmentsUseCase(DepartmentsRepositoryMock())

        /**
         * Test Get Departments
         */
        val subscriberGetDepartments:TestSubscriber<List<DepartmentEntity>> = TestSubscriber()
        getDepartmentsUseCases.getDepartments()
            .subscribeOn(Schedulers.trampoline())
            .observeOn(Schedulers.trampoline())
            .subscribe(subscriberGetDepartments)

        subscriberGetDepartments.assertNoErrors();
        subscriberGetDepartments.assertValueCount(1);
        subscriberGetDepartments.assertValue{it[0].name == "Department1"}


        /**
         * Test Get Department and how the use case count towns Number
         */
        val subscriberGetDepartment:TestSubscriber<DepartmentEntity> = TestSubscriber()
        getDepartmentsUseCases.getDepartment(subscriberGetDepartments.values()[0][0].code)
            .toFlowable()
            .subscribe(subscriberGetDepartment)


        subscriberGetDepartment.assertNoErrors();
        subscriberGetDepartment.assertValue{it.townsNumber.toInt() == 2}

    }
}