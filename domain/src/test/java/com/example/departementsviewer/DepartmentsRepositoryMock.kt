package com.example.departementsviewer

import com.example.domain.entities.DepartmentEntity
import com.example.domain.entities.TownEntity
import com.example.domain.repositories.DepartmentsRepository
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single

class DepartmentsRepositoryMock: DepartmentsRepository {
    val mDepartmentEntities = ArrayList<DepartmentEntity>()
    val mDepartmentTowns = ArrayList<TownEntity>()

    init{
        mDepartmentEntities.add(DepartmentEntity(name = "Department1", code = "01", codeRegion = 1))
        mDepartmentEntities.add(DepartmentEntity(name = "Department2", code = "02", codeRegion = 2))
        mDepartmentEntities.add(DepartmentEntity(name = "Department3", code = "03", codeRegion = 3))

        mDepartmentTowns.add(TownEntity("Town1","01","01",1,1000))
        mDepartmentTowns.add(TownEntity("Town2","02","01",1,1000))
        mDepartmentTowns.add(TownEntity("Town3","03","02",2,1000))
        mDepartmentTowns.add(TownEntity("Town4","04","03",3,1000))
    }
    override fun getDepartments(): Flowable<List<DepartmentEntity>> {
        return Flowable.fromCallable{mDepartmentEntities}
    }

    override fun getDepartment(code: String): Single<DepartmentEntity> {
        for(departmentEntity in mDepartmentEntities)
            if (departmentEntity.code == code)  return Single.just(departmentEntity)
        return Single.error(Throwable())
    }

    override fun getDepartmentTowns(code: String): Flowable<List<TownEntity>> {
        val townEntities = ArrayList<TownEntity>()
        mDepartmentTowns
            .filter{it.codeDepartment == code}
            .forEach{townEntities.add(it)}
        return Flowable.fromCallable{townEntities}
    }

    override fun saveDepartment(departmentEntity: DepartmentEntity): Completable {
        return Completable.complete()
    }
}