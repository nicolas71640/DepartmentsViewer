package com.example.departmentsviewer.entities

import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.navigation.findNavController
import com.example.departmentsviewer.R
import com.example.departmentsviewer.common.utils.closeKeyboard
import com.example.departmentsviewer.common.utils.list.ListAdapter
import com.example.departmentsviewer.common.utils.list.ListItem
import com.example.departmentsviewer.databinding.ListItemDepartmentBinding
import com.example.departmentsviewer.ui.departments_list.DepartmentsListFragmentDirections
import com.example.domain.entities.DepartmentEntity
import java.util.Comparator


data class Department(val name:String = "",val code: String = "",val codeRegion: Int = 0,val detailsSaved:Boolean = false,
                      val inhabitants: Long = 0, val townsNumber: Long = 0):ListItem {

    override fun getItemLayout(): Int = R.layout.list_item_department

    override fun setItem(holder : ListAdapter.ItemViewHolder) {
        val binding = holder.viewItemBinding
        if(binding is ListItemDepartmentBinding)
            binding.department = this
    }

    fun onClick(view: View){
        closeKeyboard(view.context,view)
        val action = DepartmentsListFragmentDirections.actionDepartmentListViewFragmentToDepartmentFragment(code)
        view.findNavController().navigate(action)
    }

    override fun compare(otherItem: ListItem): Int {
        return Comparator<Department> { department1, department2 ->
            department1.name.compareTo(department2.name)
        }.compare(this, otherItem as Department)
    }

    override fun isTheSame(otherItem: ListItem): Boolean {
        if(otherItem is Department)
            return otherItem.code == code
        return false
    }

    override fun getStringToCompare(): String = name

    companion object Mapper{
        /**
         * Map DepartmentEntity (domain entity) to Department (app entity)
         *
         * @param departmentEntity  to be mapped
         * @return  Department mapped
         */
        fun mapFromEntity(departmentEntity: DepartmentEntity):Department = Department(departmentEntity.name,departmentEntity.code,departmentEntity.codeRegion,
            departmentEntity.detailsSaved,departmentEntity.inhabitants,departmentEntity.townsNumber)

        /**
         * Map DepartmentEntity list (domain) to Department (App)
         *
         * @param departmentEntities
         * @return
         */
        fun mapFromEntities(departmentEntities: List<DepartmentEntity>):List<Department> {
            val departments: ArrayList<Department> = ArrayList()
            departmentEntities.forEach{departments.add(mapFromEntity(it))}
            return departments
        }
    }
}