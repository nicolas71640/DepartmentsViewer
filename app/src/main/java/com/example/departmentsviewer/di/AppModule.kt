package com.example.departmentsviewer.di

import androidx.room.Room
import com.example.data.api.DepartmentsApi
import com.example.data.db.DepartmentsDatabase
import com.example.data.repository.DepartmentsRepositoryImpl
import com.example.data.repository.DepartmentsLocal
import com.example.data.repository.DepartmentsRemote
import com.example.departmentsviewer.ui.department.DepartmentViewModel
import com.example.departmentsviewer.ui.departments_list.DepartmentsListViewModel
import com.example.domain.repositories.DepartmentsRepository
import com.example.domain.usecases.GetDepartmentsUseCase
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.core.qualifier.StringQualifier
import org.koin.dsl.module
import retrofit2.Retrofit

private const val BASE_URL = "https://geo.api.gouv.fr/"
private const val RETROFIT_INSTANCE = "retrofit"
private const val API_INSTANCE = "api"
private const val GET_DEPARTMENTS_USE_CASE = "getDepartmentsUseCase"
private const val REMOTE = "remote"
private const val LOCAL = "local"
private const val DATABASE_INSTANCE = "database"
private const val DATABASE_DEPARTMENTS_NAME = "departments"

/**
 * ViewModel for each fragment
 */
val mViewModels :Module = module {
    viewModel {
        DepartmentsListViewModel(
            getDepartmentsUseCase = get(
                StringQualifier(GET_DEPARTMENTS_USE_CASE)
            )
        )
    }
    viewModel {
        DepartmentViewModel(
            getDepartmentsUseCase = get(
                StringQualifier(GET_DEPARTMENTS_USE_CASE)
            )
        )
    }
}

/**
 * Repositories from Data module
 */
val mRepositories: Module = module {
    single(qualifier = StringQualifier(LOCAL)) { DepartmentsLocal(get(StringQualifier(DATABASE_INSTANCE))) }
    single(qualifier = StringQualifier(REMOTE)) { DepartmentsRemote(get(StringQualifier(API_INSTANCE))) }
    single { DepartmentsRepositoryImpl(get(StringQualifier(REMOTE)),get(StringQualifier(LOCAL))) as DepartmentsRepository }
}

/**
 * UseCases from Domain module
 */
val mUseCaseModules = module {
    factory(qualifier = StringQualifier(GET_DEPARTMENTS_USE_CASE)) { GetDepartmentsUseCase(repository = get()) }
}

/**
 * Network module using retrofit
 */
val mNetworkModules = module {
    single(qualifier = StringQualifier(RETROFIT_INSTANCE)) { createNetworkClient(BASE_URL) }
    single(qualifier = StringQualifier(API_INSTANCE)) { (get(StringQualifier(RETROFIT_INSTANCE)) as Retrofit).create(DepartmentsApi::class.java) }
}

/**
 * Local modules using room
 */
val mLocalModules = module {
    single(qualifier = StringQualifier(DATABASE_INSTANCE)) { Room.databaseBuilder(androidApplication(), DepartmentsDatabase::class.java, DATABASE_DEPARTMENTS_NAME).build() }
}


