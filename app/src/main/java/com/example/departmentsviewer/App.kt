package com.example.departmentsviewer

import android.app.Application
import com.example.departmentsviewer.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App:Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin{
            androidLogger()
            androidContext(this@App)
            modules(listOf(mViewModels, mRepositories, mUseCaseModules,mNetworkModules,mLocalModules))
        }
    }
}