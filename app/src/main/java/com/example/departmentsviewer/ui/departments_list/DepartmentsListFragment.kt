package com.example.departmentsviewer.ui.departments_list

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.departmentsviewer.common.utils.list.ListAdapter
import com.example.departmentsviewer.databinding.FragmentDepartmentListViewBinding
import com.miguelcatalan.materialsearchview.MaterialSearchView
import org.koin.androidx.viewmodel.ext.android.viewModel


/**
 * Fragment Displaying the list of Departments with a RecyclerView
 *
 */
class DepartmentsListFragment : Fragment() {
    val mViewModel: DepartmentsListViewModel by viewModel()
    lateinit var mBinding: FragmentDepartmentListViewBinding
    val mAdapter = ListAdapter()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mBinding =  FragmentDepartmentListViewBinding.inflate(inflater, container, false)
        mBinding.viewModel = mViewModel
        mBinding.lifecycleOwner = this

        mViewModel.update()

        return mBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        setRecyclerView()
        setSearchView()
    }

    private fun setRecyclerView(){
        mBinding.departmentsListView.layoutManager = LinearLayoutManager(context)
        mBinding.departmentsListView.adapter = mAdapter
    }

    private fun setSearchView(){
        mBinding.appBar.searchButton.setOnClickListener{mBinding.appBar.searchView.showSearch()}
        mBinding.appBar.searchView.closeSearch()
        val listener = object :MaterialSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(text: String?): Boolean
            {
                mAdapter.filter.filter(text)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                mAdapter.filter.filter(newText)
                return false
            }
        }

        /*Workaround to update filtering on back button pressed*/
        mBinding.appBar.searchView.setOnQueryTextListener(listener)
        mViewModel.mDepartmentsList.observe(this, Observer{ mBinding.appBar.searchView.closeSearch()})
    }

}
