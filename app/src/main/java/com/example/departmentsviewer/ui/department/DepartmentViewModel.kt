package com.example.departmentsviewer.ui.department

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.departmentsviewer.common.utils.test.EspressoIdlingResource
import com.example.departmentsviewer.common.viewmodel.DepartmentBaseViewModel
import com.example.departmentsviewer.entities.Department
import com.example.domain.usecases.GetDepartmentsUseCase
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DepartmentViewModel(getDepartmentsUseCase: GetDepartmentsUseCase): DepartmentBaseViewModel(getDepartmentsUseCase) {
    companion object {
        private val TAG: String? = DepartmentViewModel::class.simpleName
    }
    //LiveData Used by dataBinding (see fragment layouts)
    val mDepartment: MutableLiveData<Department> = MutableLiveData()

    /**
     * Set department code to then get department instance and update the LiveData
     *
     * @param departmentCode
     */
    fun setDepartmentCode(departmentCode: String) {
        val disposable = getDepartmentsUseCase.getDepartment(departmentCode)
            .flatMap {Single.just(Department.mapFromEntity(it))}
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe{ EspressoIdlingResource.increment() }
            .subscribe({ department ->
                mDepartment.postValue(department)
                EspressoIdlingResource.decrement()
            }, { error ->
                Log.e(TAG,error.toString())
            })

        addDisposable(disposable)

    }

}