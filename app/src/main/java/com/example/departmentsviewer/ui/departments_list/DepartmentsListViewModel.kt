package com.example.departmentsviewer.ui.departments_list

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.example.departmentsviewer.common.utils.test.EspressoIdlingResource
import com.example.departmentsviewer.common.viewmodel.DepartmentBaseViewModel
import com.example.departmentsviewer.entities.Department
import com.example.domain.usecases.GetDepartmentsUseCase
import io.reactivex.Flowable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class DepartmentsListViewModel(getDepartmentsUseCase: GetDepartmentsUseCase): DepartmentBaseViewModel(getDepartmentsUseCase) {
    companion object {
        private val TAG: String? = DepartmentsListViewModel::class.simpleName
    }

    val mDepartmentsList: MutableLiveData<List<Department>> = MutableLiveData()

    init{
       update()
    }

    fun update(){
        val disposable = getDepartmentsUseCase.getDepartments()
            .flatMap {Flowable.fromCallable{Department.mapFromEntities(it)}}
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe{EspressoIdlingResource.increment() }
            .subscribe({ departments ->
                mDepartmentsList.postValue(departments)
                EspressoIdlingResource.decrement()


            }, { error ->
                Log.e(TAG,error.toString())
            }, {
            })

        addDisposable(disposable)
    }
}