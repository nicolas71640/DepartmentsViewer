package com.example.departmentsviewer.ui.department

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.navigation.fragment.navArgs
import com.example.departmentsviewer.databinding.FragmentDepartmentBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

/**
 * Fragment Displaying the Info of a Department
 *
 */
class DepartmentFragment : Fragment() {
    val mViewModel: DepartmentViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        /*Binding*/
        val binding: FragmentDepartmentBinding = FragmentDepartmentBinding.inflate(inflater, container, false)
        binding.viewModel = mViewModel
        binding.appBar.toolbarUpButton.setOnClickListener({Navigation.findNavController(binding.root).navigateUp()})
        binding.lifecycleOwner = this

        /*Setting department to be displayed*/
        val args: DepartmentFragmentArgs by navArgs()
        mViewModel.setDepartmentCode(args.departmentCode)

        // Inflate the layout for this fragment
        return binding.root
    }

}
