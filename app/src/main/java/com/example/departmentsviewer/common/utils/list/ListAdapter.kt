package com.example.departmentsviewer.common.utils.list

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SortedList
import java.util.*

class ListAdapter (): RecyclerView.Adapter<ListAdapter.ItemViewHolder>(),Filterable{
    /**
     * Use of Sorted List not only to sort the list but also to update a item only if its contents changed
     * The same smooth change will happen if an item is inserted or moved
     */
    private val mSortedListCallback = object : SortedList.Callback<ListItem>() {
        override fun compare(a: ListItem, b: ListItem): Int {
            return a.compare(b)
        }

        override fun onInserted(position: Int, count: Int) {
            notifyItemRangeInserted(position, count)
        }

        override fun onRemoved(position: Int, count: Int) {
            notifyItemRangeRemoved(position, count)
        }

        override fun onMoved(fromPosition: Int, toPosition: Int) {
            notifyItemMoved(fromPosition, toPosition)
        }

        override fun onChanged(position: Int, count: Int) {
            notifyItemRangeChanged(position, count)
            notifyItemChanged(position)
        }

        override fun areContentsTheSame(oldItem: ListItem, newItem: ListItem): Boolean {
            return oldItem == newItem
        }

        override fun areItemsTheSame(item1: ListItem, item2: ListItem): Boolean {
            return item1.isTheSame(item2)
        }
    }
    private val mItemsFiltered = SortedList(ListItem::class.java, mSortedListCallback)
    private val mItems = ArrayList<ListItem>()

    inner class ItemViewHolder constructor(var viewItemBinding: ViewDataBinding) :
        RecyclerView.ViewHolder(viewItemBinding.root)

    /**
     *  Function called by the Binding Adapter "items" to refresh the data
     *
     * @param items
     */
    fun update(items: List<ListItem>) {
        mItems.clear()
        mItems.addAll(items)
        mItemsFiltered.replaceAll(items)
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val binding: ViewDataBinding
        binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            mItemsFiltered.get(0).getItemLayout(), parent, false)

        return ItemViewHolder(binding)
    }

    override fun getItemCount(): Int = mItemsFiltered.size()

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        mItemsFiltered.get(position).setItem(holder)
    }

    @Suppress("UNCHECKED_CAST")
    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(charSequence: CharSequence): FilterResults {
                val filterResults = FilterResults()
                filterResults.values =
                    if (charSequence.toString().isEmpty()) mItems
                    else ArrayList(mItems.filter {
                        it.getStringToCompare().toLowerCase(Locale.ROOT)
                            .contains(charSequence.toString().toLowerCase(Locale.ROOT))
                    })

                return filterResults
            }
            override fun publishResults(
                charSequence: CharSequence,
                filterResults: FilterResults
            ) {
                if(filterResults.values != null)
                    mItemsFiltered.replaceAll(filterResults.values as List<ListItem>)

            }
        }
    }


}