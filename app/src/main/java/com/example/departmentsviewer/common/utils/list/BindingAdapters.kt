package com.example.departmentsviewer.common.utils.list

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView

/**
 * Binding Adapter to update RecyclerView with a MutableLiveData
 * See in layouts using "items" attribute
 *
 * @param items
 */
@BindingAdapter("items")
fun RecyclerView.bindItems(items:List<ListItem>?) {
    val adapter = this.adapter
    if(adapter is ListAdapter && items != null)
        adapter.update(items)
}