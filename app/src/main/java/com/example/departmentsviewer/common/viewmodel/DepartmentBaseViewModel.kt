package com.example.departmentsviewer.common.viewmodel

import com.example.domain.usecases.GetDepartmentsUseCase

abstract class DepartmentBaseViewModel(protected val getDepartmentsUseCase: GetDepartmentsUseCase): BaseViewModel() {

}