package com.example.departmentsviewer.common.utils.list

/**
 * Interface that need to be implemented in each entity that need the ListAdapter to be displayed
 *
 */
interface ListItem {
    /**
     * @return Item Layout Id corresponding to the class of the object displayed by a ListAdapter
     */
    fun getItemLayout():Int

    /**
     *  Set the Item in the binding auto-generated
     * @param holder View Holder defined in ListAdapter class
     */
    fun setItem(holder : ListAdapter.ItemViewHolder)

    fun compare(otherItem: ListItem): Int

    fun isTheSame(otherItem: ListItem): Boolean

    fun getStringToCompare(): String


}