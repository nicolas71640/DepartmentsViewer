package com.example.departmentsviewer

import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario.launch
import androidx.test.espresso.*
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4

import org.junit.Test
import org.junit.runner.RunWith


import com.example.departmentsviewer.common.utils.test.EspressoIdlingResource
import com.example.departmentsviewer.utils.atPosition
import com.example.departmentsviewer.utils.checkSaveIconVisibility
import org.junit.After
import org.junit.Before


/**
 * Will test search, navigate to department fragment, and test if the department has been saved to the local db
 *
 */

@RunWith(AndroidJUnit4::class)
class SearchFeatureTest {
    companion object {
        private val DEPARTMENT_TO_TEST: String = "Ain"
    }

    @Before
    fun waitForDepartmentsToBeLoaded(){
        //Delete DB
        InstrumentationRegistry.getInstrumentation().targetContext.deleteDatabase("departments")
        launch(MainActivity::class.java)
        IdlingRegistry.getInstance().register(EspressoIdlingResource.countingIdlingResource)
    }

    @After
    fun unregisterIdlingResource() {
        IdlingRegistry.getInstance().unregister(EspressoIdlingResource.countingIdlingResource)
    }

    @Test
    fun testSearchViewAndSavingDepartment() {
        //Check SearchView
        onView(withId(R.id.search_button)).perform(click())
        onView(withId(R.id.searchTextView)).perform(replaceText(DEPARTMENT_TO_TEST))
        onView(withId(R.id.departmentsListView))
            .check(matches(atPosition(0, hasDescendant(withText(DEPARTMENT_TO_TEST)))))

        //Navigate to the DepartmentFragment
        onView(withId(R.id.departmentsListView)).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0,click()))
        onView(withId(R.id.toolbar_title)).check(matches(withText(DEPARTMENT_TO_TEST)))

        //Navigate up to DepartmentListFragment
        onView(withId(R.id.toolbar_up_button)).perform(click())

        //Search again for the department and check if the save icon is visible
        onView(withId(R.id.search_button)).perform(click())
        onView(withId(R.id.searchTextView)).perform(replaceText(DEPARTMENT_TO_TEST))
        onView(withId(R.id.departmentsListView)).check(matches(checkSaveIconVisibility(0)))
    }

}
