package com.example.departmentsviewer.utils

import android.view.View
import android.widget.ImageView
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.matcher.BoundedMatcher
import com.example.departmentsviewer.R
import kotlinx.android.synthetic.main.list_item_department.view.*
import org.hamcrest.Description
import org.hamcrest.Matcher


fun atPosition(position: Int, itemMatcher: Matcher<View>): Matcher<View> {
    return object : BoundedMatcher<View, RecyclerView>(RecyclerView::class.java) {
        override fun describeTo(description: Description) {
            description.appendText("has item at position $position: ")
            itemMatcher.describeTo(description)
        }

        override fun matchesSafely(view: RecyclerView): Boolean {
            val viewHolder = view.findViewHolderForAdapterPosition(position)
                ?: // has no item on such position
                return false
            return itemMatcher.matches(viewHolder.itemView)
        }
    }
}

fun checkSaveIconVisibility(position:Int):Matcher<View>{
    return object : BoundedMatcher<View, RecyclerView>(RecyclerView::class.java) {
        override fun describeTo(description: Description) {
            description.appendText("Item at $position is visible ")
        }

        override fun matchesSafely(view: RecyclerView): Boolean {
            return view.findViewById<View>(R.id.saved_icon).isVisible
        }
    }
}