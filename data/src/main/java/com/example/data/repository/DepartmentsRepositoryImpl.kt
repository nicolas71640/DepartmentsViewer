package com.example.data.repository

import com.example.data.entities.*
import com.example.domain.entities.DepartmentEntity
import com.example.domain.entities.TownEntity
import com.example.domain.repositories.DepartmentsRepository
import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.functions.BiFunction

/**
 * Implementation of DepartmentRepository
 * Can be implemented into DeparmentsUseCase (Dependancy Inversion Pattern)
 *
 * @property remote     Remote Repository (Link between Api and Deparments Repo)
 * @property local      Cache Repository (Link between Room and Deparments Repo)
 */
class DepartmentsRepositoryImpl(private val remote:DepartmentsRemote, private val local:DepartmentsLocal): DepartmentsRepository {

    /**
     * Get Departments from Remote if network available, else get it from local
     * If Got it from remote, save them to the local
     * And transform it from DepartmentData to DepartmentEntity
     *
     * @return  List of DepartmentEntity (Flowable)
     */
    override fun getDepartments(): Flowable<List<DepartmentEntity>> {

        //Get Department from api or from local if error on Network
        return Flowable.zip(remote.getDepartments(),local.getDepartments(),
            BiFunction<List<DepartmentData>?, List<DepartmentData>?, List<DepartmentData>>
            { departmentsRemote, departmentsLocal ->
                    departmentsRemote.forEach { remote ->
                        departmentsLocal.forEach { local ->
                            if (local == remote)
                                remote.mergeWithLocal(local)
                        }
                    }
                    departmentsRemote
            } )
            .doOnNext{local.saveDepartments(it)}
            .onErrorResumeNext(local.getDepartments())
            .map{DepartmentData.mapToEntities(it)}
    }

    /**
     * Get Department base on its code. Either from remote or else from local
     * Then transform it to DepartmentEntity
     * @param code  Department Code
     * @return  DepartmentEntity (Single)
     */
    override fun getDepartment(code: String): Single<DepartmentEntity> {
        return Flowable.concat(local.getDepartment(code),remote.getDepartment(code))
            .firstOrError()
            .map { DepartmentData.mapToEntity(it) }
    }

    /**
     * Get Department Towns based on department code and Transform it from TownData to TownEntity
     * Departments Towns are not saved in local
     *
     * @param code  Code Departement
     * @return  List Of TownEntity (Flowable)
     */
    override fun getDepartmentTowns(code: String): Flowable<List<TownEntity>>{
        return remote.getDepartmentTowns(code)
            .map{ TownData.mapToEntities(it)}
    }

    /**
     * Save DepartmentEntity into the local
     *
     * @param departmentEntity  DepartmentEntity to be saved in local
     * @return  Completable
     */
    override fun saveDepartment(departmentEntity: DepartmentEntity): Completable {
        return local.saveDepartment(DepartmentData.mapFromEntity(departmentEntity))
    }
}