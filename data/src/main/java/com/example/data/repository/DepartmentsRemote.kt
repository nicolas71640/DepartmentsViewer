package com.example.data.repository

import com.example.data.api.DepartmentsApi
import com.example.data.entities.DepartmentData
import com.example.data.entities.TownData
import io.reactivex.Flowable

class DepartmentsRemote(private val api: DepartmentsApi) {
    fun getDepartments(): Flowable<List<DepartmentData>> = api.getDepartments()

    fun getDepartment(code:String): Flowable<DepartmentData> = api.getDepartment(code)

    fun getDepartmentTowns(code:String): Flowable<List<TownData>> = api.getDepartmentTowns(code)
}
