package com.example.data.repository

import com.example.data.db.DepartmentsDao
import com.example.data.db.DepartmentsDatabase
import com.example.data.entities.DepartmentData
import io.reactivex.Completable
import io.reactivex.Flowable

class DepartmentsLocal(private val database: DepartmentsDatabase) {
    private val dao: DepartmentsDao = database.getDepartmentsDao()

    /**
     * Get All Departments from db
     *
     * @return  Departments list
     */
    fun getDepartments() : Flowable<List<DepartmentData>?> = dao.getAllDepartments()

    /**
     * Save Departments List into db
     *
     * @param departmentsData
     */
    fun saveDepartments(departmentsData: List<DepartmentData>) {
        dao.clear()
        dao.saveAllDepartments(departmentsData)
    }

    /**
     * Get Department from db based on its code
     *
     * @param code departmentCode
     * @return  DepartmentData (Flowable)
     */
    fun getDepartment(code: String):Flowable<DepartmentData> = dao.getDepartment(code)

    /**
     * Save Department into db
     *
     * @param departmentData
     * @return  Completable
     */
    fun saveDepartment(departmentData: DepartmentData):Completable{
        return dao.saveDepartment(departmentData)
            .doOnSubscribe{departmentData.detailsSaved = true}
            .doOnError{departmentData.detailsSaved = false}
    }


}