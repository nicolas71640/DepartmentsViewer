package com.example.data.entities

import com.example.domain.entities.TownEntity
import com.google.gson.annotations.SerializedName

/**
 * TownData got from remote, Serializable in order to use it in GSON retrofit converter
 *
 * @property name
 * @property code
 * @property codeDepartment
 * @property inhabitants
 * @property codeRegion
 */
data class TownData(@SerializedName("nom")val name: String,
                          @SerializedName("code")val code: String,
                          @SerializedName("codeDepartement")val codeDepartment: String,
                          @SerializedName("population")val inhabitants: Long,
                          @SerializedName("codeRegion")val codeRegion: Int) {

    companion object Mapper{
        /**
         * Transform TownData (Entity from data module) to TownEntity (Entity) from domain module
         *
         * @param townData  to be transformed
         * @return TownEntity
         */
        fun mapToEntity(townData: TownData):TownEntity = TownEntity(townData.name,townData.code,townData.codeDepartment,townData.codeRegion,townData.inhabitants)

        /**
         * Transform TownData list to TownEntity List
         *
         * @param townsData to be transformed
         * @return List of TownEntity
         */
        fun mapToEntities(townsData: List<TownData>):List<TownEntity> {
            val townEntities: ArrayList<TownEntity> = ArrayList()
            townsData.forEach{townEntities.add(mapToEntity(it))}
            return townEntities
        }
    }

}



