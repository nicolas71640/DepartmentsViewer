package com.example.data.entities

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.domain.entities.DepartmentEntity
import com.google.gson.annotations.SerializedName
import java.util.*
import kotlin.collections.ArrayList

/**
 * DepartmentData entity got from local or remote
 * Stored in database thanks to room (tableName = departments)
 * Converted from Json thanks to retrofit converter
 *
 * @property name
 * @property code
 * @property codeRegion
 * @property detailsSaved
 * @property inhabitants
 * @property townsNumber
 */
@Entity(tableName = "departments")
data class DepartmentData(@PrimaryKey @NonNull @SerializedName("nom")val name: String = "",
                          @SerializedName("code")val code: String = "",
                          @SerializedName("codeRegion")val codeRegion: Int = 0,
                          @SerializedName("detailsSaved")var detailsSaved: Boolean = false,
                          @SerializedName("inhabitants")var inhabitants: Long = 0,
                          @SerializedName("townsNumber") var townsNumber: Long = 0) {

    /**
     * Merge Remote data to cache data. Especially here to update the remote one with inhabitants and townsNumber
     * if those details have been already saved in db
     *
     * @param departmentDataLocal
     */
    fun mergeWithLocal(departmentDataLocal: DepartmentData)
    {
        if(departmentDataLocal.detailsSaved){
            inhabitants = departmentDataLocal.inhabitants
            townsNumber = departmentDataLocal.townsNumber
        }
        detailsSaved = departmentDataLocal.detailsSaved
    }

    override fun equals(other: Any?): Boolean {
        if(other is DepartmentData)
            return code.equals(other.code)
        return false
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }


    companion object Mapper{
        /**
         * Convert DepartmentData (data module entity) to DepartmentEntity (Domain module Entity)
         *
         * @param departmentData to be converted
         * @return DepartmentEntity
         */
        fun mapToEntity(departmentData: DepartmentData):DepartmentEntity = DepartmentEntity(departmentData.name,departmentData.code,departmentData.codeRegion,departmentData.detailsSaved,departmentData.inhabitants,departmentData.townsNumber)

        /**
         * Convert List of DepartmentData to a list DepartementEntity
         *
         * @param departmentsData List of DepartmentData to be converted
         * @return  List of DepartmentEntity (to be received by domain module)
         */
        fun mapToEntities(departmentsData: List<DepartmentData>):List<DepartmentEntity> {
            val departmentEntities: ArrayList<DepartmentEntity> = ArrayList()
            departmentsData.forEach{departmentEntities.add(mapToEntity(it))}
            return departmentEntities
        }

        /**
         * Map DepartmentEntity (domain module entity) to DepartmentData (data module entity)
         *
         * @param departmentEntity to be converted to DepartmentData
         * @return  DepartementData
         */
        fun mapFromEntity(departmentEntity: DepartmentEntity):DepartmentData = DepartmentData(name = departmentEntity.name,code = departmentEntity.code,
            codeRegion = departmentEntity.codeRegion,inhabitants = departmentEntity.inhabitants,townsNumber =  departmentEntity.townsNumber)

    }
}





