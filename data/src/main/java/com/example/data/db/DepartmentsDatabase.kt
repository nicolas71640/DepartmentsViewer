package com.example.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.data.entities.DepartmentData

@Database(entities = arrayOf(DepartmentData::class), version = 1, exportSchema = false)
abstract class DepartmentsDatabase : RoomDatabase() {
    abstract fun getDepartmentsDao(): DepartmentsDao
}