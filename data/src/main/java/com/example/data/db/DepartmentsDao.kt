package com.example.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.data.entities.DepartmentData
import io.reactivex.Completable
import io.reactivex.Flowable

@Dao
interface DepartmentsDao {

    @Query("Select * from departments")
    fun getAllDepartments(): Flowable<List<DepartmentData>?>

    @Query("Select * from departments where code = :code")
    fun getDepartment(code :String): Flowable<DepartmentData>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveAllDepartments(departments: List<DepartmentData>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveDepartment(department: DepartmentData):Completable

    @Query("DELETE FROM departments")
    fun clear()
}