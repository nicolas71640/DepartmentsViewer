package com.example.data.api

import com.example.data.entities.DepartmentData
import com.example.data.entities.TownData
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Path

interface DepartmentsApi {

    @GET("departements")
    fun getDepartments(): Flowable<List<DepartmentData>>

    @GET("departements/{code}")
    fun getDepartment(@Path("code") code:String): Flowable<DepartmentData>

    @GET("departements/{code}/communes")
    fun getDepartmentTowns(@Path("code") code:String): Flowable<List<TownData>>
}